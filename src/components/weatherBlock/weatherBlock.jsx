import React from 'react';

const WeatherBlock = ({
    type,
    celcius = 0,
    fahrenheit = 37,
    icon = '01d',
    desc = 'No Data',
    day = 'No Data'
}) => (
    <div className={`weatherBlock weatherBlock--${type}`}>
        <div className="weatherBlock__dataWrapper">
            <span className="weatherBlock__item weatherBlock__item--day">{day}</span>
            <span className="weatherBlock__item weatherBlock__item--icon">
                <img src={`icons/${icon}.svg`} alt={`Icon showing ${desc}`}/>
            </span>
            <span className="weatherBlock__item weatherBlock__item--desc">{desc}</span>
            <span className="weatherBlock__item weatherBlock__item--tempMain">{celcius} 
                <span className="weatherBlock__item weatherBlock__item--tempSymbol">&#8451;</span>
            </span>
            <span className="weatherBlock__item weatherBlock__item--tempSec">{fahrenheit} &#8457;</span>
        </div>
        <img className="weatherBlock__backgroundIcon" src={`icons/${icon}.svg`} alt={`Background Icon showing ${desc}`}/>
    </div>
)

export default WeatherBlock;
