import WeatherBlock from './weatherBlock';

describe('weatherBlock', () => {
    const props = {
        type: 'testType',
        celcius: 18,
        fahrenheit: 37,
        icon: '01d',
        desc: 'Test Description',
        day: 'Test'
    }
    const wrapper = mount(<WeatherBlock {...props} />);

    describe('Render', () => {
        it('Should render the weather block', () => {
            expect(wrapper.type()).toBe(WeatherBlock);
        });
    });

    describe('Render with props', () => {
        it('Should render the weather block with props', () => {
            expect(wrapper.props()).toEqual({
                type: 'testType',
                celcius: 18,
                fahrenheit: 37,
                icon: '01d',
                desc: 'Test Description',
                day: 'Test'
            });
        });
    });

});
