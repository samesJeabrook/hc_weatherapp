import React from 'react';

const SearchBar = ({setLocation}) => {

    const [value, setValue] = React.useState('');
    
    function onSubmit(e){
        e.preventDefault
        value !== '' && setLocation(value);
    }

    return(
        <form
            onSubmit={(e) => onSubmit(e)}
            className="form"
        >
            <input
                type="search"
                className="form__search"
                placeholder="London, GB"
                aria-label="Search"
                onChange={(e) => setValue(e.target.value)}/>
            <button
                type="submit"
                className="form__submit"
            >Find</button>
        </form>
    )
}

export default SearchBar;
