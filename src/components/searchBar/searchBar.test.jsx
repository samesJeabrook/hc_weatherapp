import SearchBar from './searchBar';

describe('SearchBar', () => {
    const wrapper = mount(<SearchBar />);

    it('Should render the search bar', () => {
        expect(wrapper.type()).toBe(SearchBar);
    });
});
