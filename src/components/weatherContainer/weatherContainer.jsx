import React from 'react';

import {settings} from '../../__config__/settings';
import {parseWeather} from '../../__helpers__/parsers';

import {WeatherBlock} from '../'

const WeatherContainer = ({weatherData, displayDays}) => {

    function displayWeatherBlocks(){
        const filteredData = parseWeather(weatherData);
        return filteredData.map((data, i) => {
            if(i+1 <= displayDays){
                return(
                    <WeatherBlock
                        key={data.day.date}
                        day={`${data.day.text} ${data.day.date}`}
                        type={data.type}
                        icon={data.icon}
                        desc={data.desc}
                        celcius={data.temp.celcius}
                        fahrenheit={data.temp.fahrenheit}
                    />
                )
            }
        })
    }

    return (
        <div className="weatherContainer">
            {displayWeatherBlocks()}
        </div>
    );
}

export default WeatherContainer;
