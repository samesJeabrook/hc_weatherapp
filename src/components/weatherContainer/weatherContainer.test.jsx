import WeatherContainer from './weatherContainer';

describe('WeatherContainer', () => {

    const data = {
        data:{
            list: []
        }
    }

    const wrapper = mount(<WeatherContainer weatherData={data} />);

    it('Should render the search bar', () => {
        expect(wrapper.type()).toBe(WeatherContainer);
    });
});
