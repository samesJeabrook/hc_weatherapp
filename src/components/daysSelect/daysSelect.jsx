import React from 'react';

const DaysSelect = ({setDays, value}) => (
        <select 
            className="select"
            onChange={setDays} 
            value={value}
            aria-label="Select Days"
        >
            <option value={1}>1 day</option>
            <option value={2}>2 days</option>
            <option value={3}>3 days</option>
            <option value={4}>4 days</option>
            <option value={5}>5 days</option>
        </select>
    )

export default DaysSelect;
