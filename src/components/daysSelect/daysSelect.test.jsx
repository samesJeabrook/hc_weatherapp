import DaysSelect from './daysSelect';

describe('DaysSelect', () => {
    const wrapper = mount(<DaysSelect />);

    it('Should render the days selection', () => {
        expect(wrapper.type()).toBe(DaysSelect);
    });
});
