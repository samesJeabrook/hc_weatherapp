export const settings = {
    CACHE_HOURS: 3,
    DAYS_SHOWING: 4,
    DEFAULT_LOCATION: 'LONDON_GB'
}
