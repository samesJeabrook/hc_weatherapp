import {keys} from './keys';

export const url = (city, country) => `https://api.openweathermap.org/data/2.5/forecast?q=${city},${country}&mode=json&appid=${keys.api}`;
