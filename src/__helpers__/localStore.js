import {settings} from '../__config__/settings';

export const store = {
    set: (city, country, data) => {
        const time = new Date().getTime();
        const locale = `${city}_${country}`;
        const obj = {
            timeStamp: time,
            data
        }
        localStorage.setItem(locale, JSON.stringify(obj));
    },

    get: (city, country) => {
        const time = new Date().getTime();
        const locale = `${city}_${country}`;
        if(localStorage[locale]){
            const storeData = JSON.parse(localStorage[locale]);
            if(time - storeData.timeStamp < settings.CACHE_HOURS*60*60*1000){
                return storeData;
            }
            return false;
        }
        return false;
    },

    setPageLocale: (location) => {
        const locale = location.toUpperCase().split(',');
        localStorage.setItem('PageLocation',`${locale[0]}_${locale[1].trim()}`)
    }
}
