import "babel-polyfill";
import axios from 'axios';

import {url} from '../__config__/apiUrl';
import {settings} from '../__config__/settings';

export const request = async (city, country, config) => {
    try {
        const { data } = await axios.request({
            method: 'get',
            url: url(city, country),
            ...config
        });

        return data;
    } catch (e) {
        console.warn('Could not fetchData', e);
        const location = settings.DEFAULT_LOCATION.split('_');
        request(location[0], location[1]);
    }
};
