const parseData = (item) => {
    return{
        day: parseDate(item.dt),
        temp: {
            celcius: parseCelcius(item.main.temp),
            fahrenheit: parseFahrenheit(item.main.temp)
        },
        desc: item.weather[0].description,
        icon: parseIcon(item.weather[0].icon),
        type: parseType(item.weather[0].description)
    };
};

const parseDate = (unixTime) => {
    const date = new Date(unixTime * 1000);
    const currentDate = new Date();
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const parsedDay = date.getDay();
    const parsedDate = date.getDate();
    const text = parsedDay === currentDate.getDay() ? 'Today' : days[parsedDay]
    let ordinal = 'th';
    if(parsedDate === 1) ordinal = 'st';
    if(parsedDate === 2) ordinal = 'nd';
    if(parsedDate === 3) ordinal = 'rd';
    return {
        text,
        date: (parsedDate)+ordinal,
        time: date.getHours()
    };
};

const parseType = (str) => {
    str = str.replace(/[-_\s.]+(.)?/g, (_, c) => c ? c.toUpperCase() : '');
    return str.substr(0, 1).toLowerCase() + str.substr(1);
}

const parseCelcius = (temp) => {
    return Math.round(temp - 273.15);
};

const parseFahrenheit = (temp) => {
    return Math.round((temp * 1.8) - 459.67);
};

const parseIcon = (icon) => {
    const parsedIcon = icon.replace(/[a-z]/, 'd')
    return parsedIcon;
};

export const parseWeather = (weatherData) => {
    const {list} = weatherData.data;
    let returnData = [];
    let parsedToday = false;
    list.map(item => {
        const parsedData = parseData(item);
        const currentTime = new Date().getHours();
        if(!parsedToday && parsedData.day.text === 'Today' && currentTime > 12){
            parsedToday = true;
            returnData.push(parsedData);
        }
        if(parsedData.day.time === 12){
            returnData.push(parsedData);
        }
    });
    return returnData;
};
