import React from 'react';
import ReactDOM from 'react-dom';

import {request} from './__helpers__/api';
import {store} from './__helpers__/localStore';
import {settings} from './__config__/settings';

import {SearchBar, WeatherContainer, DaysSelect} from './components';

import './styles/app.scss';

const App = () => {

    const [location, setLocation] = React.useState(settings.DEFAULT_LOCATION);
    const [days, setDays] = React.useState(settings.DAYS_SHOWING);
    const [weatherData, setWeatherData] = React.useState();

    React.useEffect(() => {
        let actualLocation = localStorage['PageLocation'] ? localStorage['PageLocation'] : location;
        actualLocation = actualLocation.split('_');
        getPageData(actualLocation[0], actualLocation[1]);
    },[location]);

    const setPageLocation = ((value) => {
        setLocation(value)
        store.setPageLocale(value);
    });

    const setDaysShowing = ((e) => {
        setDays(e.target.value);
    })

    const getPageData = (city, country) => {
        if(store.get(city, country)){
            const data = store.get(city, country);
            data.data && setWeatherData(data);
        }else{
            request(city, country).then((data) => {
                data && setWeatherData({
                    data
                });
                store.set(city, country, data);
            });
        }
    }

    const isAdmin = () => {
        const url = window.location.href;
        const actualUrl = decodeURI(url);
        return actualUrl.includes('#admin')
    }

    return(
        <div className="container">
            {isAdmin() && <SearchBar setLocation={setPageLocation} />}
            {weatherData && <WeatherContainer weatherData={weatherData} displayDays={days} />}
            {isAdmin() && <DaysSelect setDays={setDaysShowing} value={days} />}
        </div>
    )
};

ReactDOM.render( <App />, document.getElementById('root'));
