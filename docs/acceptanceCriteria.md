# User Stories

1. As a Content Editor I want to add a weather control to a page and select the location of interest so that users can see the upcoming weather for that location.

2. As a Content Editor I would like to control the number of days of weather to be shown in the control so that users can see a suitable long or short range forecast.

3. As an Anonymous User visiting the London page I would like to see the current weather for London, UK so that I know what is currently happening.

4. As an Anonymous User visiting the London page I would like to view the upcoming weather in London, UK so that I can plan my trip appropriately.

5. As an Anonymous User visiting the London page I would like to know the day and date for all weather points so that I know the information is current and relevant.

6. As an Anonymous User visiting the London page I would like to see the range of temperatures for each day shown.

7. As an Anonymous User visiting the London page I would like to see icons for the main weather feature for each day shown so that I can tell what the likely weather is at a glance.

# Acceptance Critera

## User Story 1

As a Content Editor I want to add a weather control to a page and select the location of interest so that users can see the upcoming weather for that location.

## Scenario:

Content Editor is logged in and editing what weather location should be displayed on given page.

## Acceptance Critera 1

__Given:__ User is Content Editor

__And:__ User has navigated to specific content page

__And:__ User has *not* previously edited this page

__Then:__ Default placeholder content is displayed

__When:__ User types within search panel

__Then:__ A list of suggested cities is presented as user types

__And:__ User selects city from list

__Then:__ City data is displayed

## Acceptance Critera 2

__Given:__ User is Content Editor

__And:__ User has navigated to specific content page

__And:__ User *has* previously edited this page

__Then:__ Previous selected weather is displayed

__When:__ User types within search panel

__Then:__ A list of suggested cities is presented as user types

__And:__ User selects city from list

__Then:__ City data is displayed

***

## User Story 2

As a Content Editor I would like to control the number of days of weather to be shown in the control so that users can see a suitable long or short range forecast.

## Scenario:

Content Editor is logged in and editing the length of weather content to be displayed on given page.

## Acceptance Critera 1

__Given:__ User is Content Editor

__And:__ User has navigated to specific content page

__And:__ User has *not* previously edited this page

__Then:__ Default number of days is displayed

__When:__ User selects number of days to display

__Then:__ Content adjusts to show the correct length of content

## Acceptance Critera 2

__Given:__ User is Content Editor

__And:__ User has navigated to specific content page

__And:__ User *has* previously edited this page

__Then:__ Previously selected number of days is displayed

__When:__ User selects a new number of days to display

__Then:__ Content adjusts to show the correct length of content

***

## User Story 3

As an Anonymous User visiting the London page I would like to see the current weather for London, UK so that I know what is currently happening.

## Scenario:

Anonymous User is viewing the London weather page.

## Acceptance Critera 1

__Given:__ User is *not* content editor

__And:__ User has navigated to the London page

__Then:__ The London weather for the current day should be displayed.

***

## User Story 4

As an Anonymous User visiting the London page I would like to view the upcoming weather in London, UK so that I can plan my trip appropriately.

## Scenario:

Anonymous User is viewing the London weather page.

## Acceptance Critera 1

__Given:__ User is *not* content editor

__And:__ User has navigated to the London page

__Then:__ The London weather and previously (content editor) selected length of forecast should be displayed.

***

## User Story 5

As an Anonymous User visiting the London page I would like to know the day and date for all weather points so that I know the information is current and relevant.

## Scenario:

Anonymous User is viewing the London weather page.

## Acceptance Critera 1

__Given:__ User is *not* content editor

__And:__ User has navigated to the London page

__Then:__ The Day and date should be displayed with each forecast element

***

## User Story 6

As an Anonymous User visiting the London page I would like to see the range of temperatures for each day shown.

## Scenario:

Anonymous User is viewing the London weather page.

## Acceptance Critera 1

__Given:__ User is *not* content editor

__And:__ User has navigated to the London page

__Then:__ The days temperature be displayed adequately with each forecast element, both in centigrade and farenheight. preference to centigrade should be apparent.

***

## User Story 7

As an Anonymous User visiting the London page I would like to see icons for the main weather feature for each day shown so that I can tell what the likely weather is at a glance.

## Scenario:

Anonymous User is viewing the London weather page.

## Acceptance Critera 1

__Given:__ User is *not* content editor

__And:__ User has navigated to the London page

__Then:__ Relevant weather icon should be displayed within forecast block

