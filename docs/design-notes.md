﻿# Weather app design notes

The design team have provided us with a set to gradient colours, fonts and icon assets with which to build the app.

## Colours

*Gradients by weather type:*

| Weather Type     | Colour 1   | Colour 2   |
|------------------|------------|------------|
| Clear sky        | `#ffdb4e`  | `#ff9872`  |
| Few clouds       | `#03B9A5`  | `#30ECEB`  |
| Scattered clouds | `#3E93B2`  | `#44EDEC`  |
| Broken clouds    | `#3E93B2`  | `#44EDEC`  |
| Shower rain      | `#066E94`  | `#6fd4fb`  |
| Rain             | `#24A0CD`  | `#6fd4fb`  |
| Thunderstorm     | `#0A465D`  | `#61A1BA`  |
| Snow             | `#74A3DE`  | `#DCEDF4`  |
| Mist             | `#919494`  | `#D6D8D9`  |

## Fonts

Roboto (https://fonts.google.com/specimen/Roboto)

Roboto Condensed (https://fonts.google.com/specimen/Roboto+Condensed)

## API Key

Please use the following key for OpenWeatherMap: `6ee4d7f2febc5519e05f2a7102962128`
