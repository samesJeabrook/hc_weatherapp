# Tasks

Tasks have been broken down by dermining the nature of the task. The 1st number in the task number is associated with its user story, if the number starts with 0 then this is because it is associated to project set up and not part of a story.

***

## 0001
### *Set up project webpack and associated build scripts*
- [X] Create package.json file
- [X] set up linting and testing tools

***

## 1001
### *Determine if user is a content admin*
- [x] Create means to determine if user is a Content Editor and can access additional content

## 1002
### *Create Ajax functions*
- [x] Create function that calls API
- [x] Set up frontend caching

***

## 1003
### *Create input component*
- [x] Create visual UI as per spec provided
- [x] Connect input value to gather to search for displaying cities <- unsupported by API
- [x] Selecting a city saves the current location to the page

***

## 1004
### *Create basic output component to display data from selected city*

- [x] Display data from chosen city

***

## 2001
### *Create select component*
*Not in scope for this project*
- [ ] Create select component for selecting the amount of days to show weather content
- [ ] Selecting length saves selection and adjusts output data to reflect length

***

## 3001
### *Create weather display block*
- [x] Show default block and styling when no data is selected to show.

***

## 3002
### *Add specific weather styling*
- [x] Output colour gradient that matches weather type
- [x] Output weather text for the current day in correct font

***

## 4001
### *Display upcoming weather*
*Duplicate task linked with 3002*
- [X] Update weather blocks to show upcoming weather in correct styles.

***

## 5001
### *Display day and date with in weather block*
- [x] Create timestamp conversion util
- [x] Update blocks to show day and date with relevant styles and text output

***

## 6001
### *Display tempreture for given forecast*
- [ ] Create Kelvin to Celcius and Farenheit converstion utils
- [ ] Update blocks to show the tempretures with relevant styles

***

## 7001
### *Display Icons relevant to the weather output*
- [ ] Update forecast blocks to show relebvant Icons determined by returned data.
