# User Stories

1. As a Content Editor I want to add a weather control to a page and select the location of interest so that users can see the upcoming weather for that location.

2. As a Content Editor I would like to control the number of days of weather to be shown in the control so that users can see a suitable long or short range forecast.

3. As an Anonymous User visiting the London page I would like to see the current weather for London, UK so that I know what is currently happening.

4. As an Anonymous User visiting the London page I would like to view the upcoming weather in London, UK so that I can plan my trip appropriately.

5. As an Anonymous User visiting the London page I would like to know the day and date for all weather points so that I know the information is current and relevant.

6. As an Anonymous User visiting the London page I would like to see the range of temperatures for each day shown.

7. As an Anonymous User visiting the London page I would like to see icons for the main weather feature for each day shown so that I can tell what the likely weather is at a glance.
