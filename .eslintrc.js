module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "globals": {
        "describe": false,
        "test": true,
        "jest": true,
        "CQ": true,
        "sophus": true,
        "it": false,
        "expect": false,
        "React": false,
        "beforeEach": false,
        "beforeAll": false,
        "mount": false,
        "shallow": false,
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "react/prefer-es6-class": "error",
        "react/no-unknown-property": "error",
        "react/no-redundant-should-component-update": "error",
        "react/no-is-mounted": "error",
        "react/no-deprecated": "error",
        "react/no-array-index-key": 0,
        "react/sort-comp": "error",
        "react/no-typos": "error",
        "react/no-direct-mutation-state": "error",
        "react/jsx-no-undef": "error",
        "react/jsx-no-duplicate-props": "error",
        "react/jsx-no-comment-textnodes": "error",
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
    }
};