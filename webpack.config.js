const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        main: path.resolve(__dirname, './src/app.jsx')
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'public/dist')
    },
    module: {
        strictExportPresence: true,
        rules: [
            {               
                test: /\.(js|jsx)$/,
                use: {
                    loader: require.resolve('babel-loader'),
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: [
                            '@babel/plugin-proposal-object-rest-spread',
                            'transform-export-extensions'
                        ],
                        exclude: /node_modules/
                    },
                }
            },
            {
                test: /\.(css|scss)$/,
                loader: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options:{
                            sourceMap: true
                        }
                    }
                ]
            }
        ]              
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.scss']
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                sourceMap: true,
            }),
        ],
      },
    devtool: 'cheap-module-eval-source-map'
}
