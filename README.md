# Welcome to my weather app

This project was creating as part a way of showing how I would lead a small project from user stories through to tasks.

I have created acceptance critera and tasks from the user stories provided, the relevant documentation can be found here:

* [User Stories](/docs/userStories.md)
* [Accdeptance Critera](/docs/acceptanceCriteria.md)
* [Tasks](/docs/tasks.md)

## Getting Started

This is a purely frontend example so there will be no need for servers to run, however you will need to create the relevant build files by running the deploy script after installing all modules.

```
npm i && npm run build
```

Once you have everything installed and built you then need to run:

```
npm start
```
This will run a basic server on port 3000, head to localhost:3000 to see the site.

Because the brief made mention of a "content editor" I have also had hashbang of `#admin` to see the administrator only content for editing the page.

## Running tests

There are several tests I have set up for basic linting, both style and js which can be run via the commands:

```
npm run test:lint:js 
npm run test:lint:styled 
```

The main project Jest tests can be run using the command

```
npm run test
```

Tests can be watched with `npm run test:watch` and test coverage from `npm run test:jest:coverage`.

