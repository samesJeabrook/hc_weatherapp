import React from 'react';
import {
	shallow, mount
} from 'enzyme';

global.React = React;
global.shallow = shallow;
global.mount = mount;
